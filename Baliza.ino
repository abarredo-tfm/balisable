#include <ArduinoBLE.h>

#include <ArduinoECCX08.h>
#include <utility/ECCX08JWS.h>
#include <ECCX08.h>

#include "I2Cdev.h"
#include "MPU6050.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

MPU6050 accelgyro;

int16_t ax, ay, az;
int16_t gx, gy, gz;


int advertisingTime = 5000;
String beaconPassword = "";

BLEService batteryService("180F");
BLEService dataService("7777");


BLEUnsignedCharCharacteristic batteryLevelChar("2A19", BLERead | BLENotify);
BLEStringCharacteristic accelChar("6666", BLERead | BLENotify, advertisingTime);
BLEStringCharacteristic gyroChar("5555", BLERead | BLENotify, advertisingTime);
BLEStringCharacteristic statusChar("4444", BLERead | BLENotify, advertisingTime);

BLEStringCharacteristic authChar("0000", BLEWrite, advertisingTime);
BLEBoolCharacteristic authStatusChar("0001", BLERead);

int oldBatteryLevel = 0;  // last battery level reading from analog input
long previousMillis = 0;  // last time the battery level was checked, in ms

String header = "{\"alg\":\"ES256\",\"typ\":\"JWS\",\"beaconId\":\"19441\"}";

void setup() {

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif

  Serial.begin(9600);    // initialize serial communication
//  while (!Serial);


  pinMode(LED_BUILTIN, OUTPUT); // initialize the built-in LED pin to indicate when a central is connected

  BLE.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  BLE.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);

  if (!ECCX08.begin()) {
    Serial.println("No ECCX08 present!");
    while (1);
  }

  // begin initialization
  if (!BLE.begin()) {
    Serial.println("starting BLE failed!");

    while (1);
  }

  accelgyro.initialize();

  BLE.setDeviceName("Beacon 19441");
  BLE.setLocalName("Beacon 19441");

  BLE.setAdvertisedService(batteryService); // add the service UUID
  batteryService.addCharacteristic(batteryLevelChar); // add the battery level characteristic
  BLE.addService(batteryService); // Add the battery service
  batteryLevelChar.writeValue(oldBatteryLevel); // set initial value for this characteristic

//  BLE.setAdvertisedService(dataService);
  dataService.addCharacteristic(accelChar);
  dataService.addCharacteristic(gyroChar);
  dataService.addCharacteristic(statusChar);
  dataService.addCharacteristic(authChar);
  dataService.addCharacteristic(authStatusChar);
  BLE.addService(dataService);

  // start advertising
  BLE.advertise();

  resetChars();

  Serial.println("Bluetooth device active, waiting for connections...");
}

void loop() {
  // wait for a BLE central
  BLEDevice central = BLE.central();

  // if a central is connected to the peripheral:
  if (central) {
    if (authChar.value() == beaconPassword) {
      authStatusChar.writeValue(true);
    } else {
      authStatusChar.writeValue(false);
    }

    // while the central is connected:
    while (authStatusChar.value() && central.connected()) {
      long currentMillis = millis();
      int rssi = 0; //central.rssi();
      // if 200ms have passed, check the battery level:
      if (currentMillis - previousMillis >= advertisingTime) {
        previousMillis = currentMillis;
        updateBatteryLevel();
        updateAccel();
        updateGyro();
        updateStatus(rssi);
      }
    }
  }
}

void blePeripheralConnectHandler(BLEDevice central) {
  // central connected event handler
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.print("Connected event, central: ");
  Serial.println(central.address());
}

void blePeripheralDisconnectHandler(BLEDevice central) {
  // central disconnected event handler
  resetChars();
  digitalWrite(LED_BUILTIN, LOW);
  Serial.print("Disconnected event, central: ");
  Serial.println(central.address());
}

void resetChars() {
  authChar.writeValue("-");
  batteryLevelChar.writeValue(0);
  accelChar.writeValue("-");
  gyroChar.writeValue("-");
  statusChar.writeValue("-");
  authStatusChar.writeValue(false);
}


void updateBatteryLevel() {
  int battery = analogRead(A0);
  int batteryLevel = map(battery, 0, 1023, 0, 100);

  if (batteryLevel != oldBatteryLevel) {      // if the battery level has changed
    batteryLevelChar.writeValue(batteryLevel);  // and update the battery level characteristic
    oldBatteryLevel = batteryLevel;           // save the level for next comparison
  }
}

void updateAccel() {
  accelgyro.getAcceleration(&ax, &ay, &az);

  char accelBuff[100];
  sprintf(accelBuff, "{\"accelX\":\"%i\",\"accelY\":\"%i\",\"accelZ\":\"%i\"}", ax, ay, az);
  String accel = String(accelBuff);

  String jws = ECCX08JWS.sign(0, header, accel);
  accelChar.writeValue(jws);
}

void updateGyro() {
  accelgyro.getRotation(&gx, &gy, &gz);

  char gyroBuff[100];
  sprintf(gyroBuff, "{\"gyroX\":\"%i\",\"gyroY\":\"%i\",\"gyroZ\":\"%i\"}", gx, gy, gz);
  String gyro = String(gyroBuff);

  String jws = ECCX08JWS.sign(0, header, gyro);
  gyroChar.writeValue(jws);
}

void updateStatus(int rssi) {
  char statusBuff[100];
  sprintf(statusBuff, "{\"uptime\":\"%i\", \"rssi\":\"%i\"}", millis(), rssi);
  String statusData = String(statusBuff);

  String jws = ECCX08JWS.sign(0, header, statusData);
  statusChar.writeValue(jws);
}
